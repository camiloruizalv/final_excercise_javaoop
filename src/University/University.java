package University;

import University.Course.Course;
import University.User.Student;
import University.User.Teacher;
import University.User.fullTimeTeacher;
import University.User.partTimeTeacher;

import java.util.ArrayList;
import java.util.List;

public class University {

    private List<Teacher> teachers = new ArrayList<Teacher>();
    private List<Course> courses = new ArrayList<Course>();
    private List<Student> students = new ArrayList<Student>();

    public void createFullTimeTeacher(String name, double base_salary, int years_of_experience){
        fullTimeTeacher ftteacher = new fullTimeTeacher(name, base_salary, years_of_experience);
        this.teachers.add(ftteacher);
    }

    public void createPartTimeTeacher(String name, double base_salary, int active_hours){
        partTimeTeacher ptteacher = new partTimeTeacher(name, base_salary, active_hours);
        this.teachers.add(ptteacher);
    }

    public void createCourse(String name, int assignedCourseRoom, Teacher teacher){
        this.courses.add(new Course(name, assignedCourseRoom, teacher));
    }

    public void addStudentToCourse(int idstudent, int idcourse) {
        Student studenttoadd = this.students.get(idstudent);
        Course courseselected = this.courses.get(idcourse);

        courseselected.addStudent(studenttoadd);
    }

    public String courseToString(int idcourse) {
        return "Id course: " + courses.get(idcourse).getId() + "; Name: " + courses.get(idcourse).getName() + "; Assigned teacher: " + courses.get(idcourse).getTeacher().getName();
    }

    public String studentInCourse(int idcourse, int idstudentincourse){
        Course courseselected = this.courses.get(idcourse);

        return "Id: " + courseselected.getStudents().get(idstudentincourse).getId() + "; Name: " + courseselected.getStudents().get(idstudentincourse).getName();
    }

    public int amountOfStudentsInCourse(int idcourse){
        return courses.get(idcourse).getStudents().size();
    }

    public void createStudent(String name, int age){
        this.students.add(new Student(name, age));
    }

    public String studentToString(int idstudent){
        return "Id: " + this.students.get(idstudent).getId() + "; Name: " + this.students.get(idstudent).getName() + "; Age: " + this.students.get(idstudent).getAge();
    }

    public List<Teacher> getTeachers() {
        return teachers;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public List<Student> getStudents() {
        return students;
    }
}
