package University.User;

import University.Security.idStudentGenerator;

public class Student {
    private int id;
    private String name;
    private int age;

    public Student(String name, int age){
        idStudentGenerator idgenerated = new idStudentGenerator();
        this.name = name;
        this.age = age;
        this.id = idgenerated.getId();
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public int getAge() {
        return this.age;
    }
}
