package University.User;

import University.Security.idTeacherGenerator;

public class partTimeTeacher extends Teacher{
    private int id;
    private String position = "Part";
    private int active_hours;
    private String name;
    private double base_salary;


    public partTimeTeacher(String name, double base_salary, int active_hours) {
        super(name, base_salary);
        idTeacherGenerator idgenerated = new idTeacherGenerator();
        this.name = name;
        this.base_salary = base_salary;
        this.active_hours = active_hours;
        this.id = idgenerated.getId();
    }

    @Override
    public double calcSalary() {
        return this.active_hours*this.base_salary;
    }

    @Override
    public int getSalaryMultiplier() {
        return this.active_hours;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public double getBase_salary() {
        return this.base_salary;
    }

    @Override
    public String getPosition() {
        return this.position;
    }

    @Override
    public int getId(){
        return this.id;
    }
}
