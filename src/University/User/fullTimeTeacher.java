package University.User;

import University.Security.idTeacherGenerator;

public class fullTimeTeacher extends Teacher{
    private int id;
    private String position = "Full";
    private int years_of_experience;
    private String name;
    private double base_salary;

    public fullTimeTeacher(String name, double base_salary, int years_of_experience) {
        super(name, base_salary);
        idTeacherGenerator idgenerated = new idTeacherGenerator();
        this.name = name;
        this.base_salary = base_salary;
        this.years_of_experience = years_of_experience;
        this.id = idgenerated.getId();
    }

    @Override
    public double calcSalary() {
        return this.years_of_experience*this.base_salary;
    }

    @Override
    public int getSalaryMultiplier() {
        return this.years_of_experience;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public double getBase_salary() {
        return this.base_salary;
    }

    @Override
    public String getPosition() {
        return this.position;
    }

    @Override
    public int getId() {
        return id;
    }
}
