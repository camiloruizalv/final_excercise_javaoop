package University.User;

public abstract class Teacher {
    private int id;
    private String name;
    private double base_salary;
    private String position;

    public Teacher(String name, double base_salary){
        this.name = name;
        this.base_salary = base_salary;
    }

    public abstract double calcSalary();
    public abstract String getName();
    public abstract double getBase_salary();
    public abstract String getPosition();
    public abstract int getSalaryMultiplier();
    public abstract int getId();
}
