package University.Course;

import University.Security.idCourseGenerator;
import University.User.Student;
import University.User.Teacher;

import java.util.ArrayList;
import java.util.List;

public class Course {
    private int id;
    private String name;
    private int assignedCourseRoom;
    private List<Student> students = new ArrayList<Student>();
    private Teacher teacher;

    public Course(String name, int assignedCourseRoom, Teacher teacher){
        idCourseGenerator idgenerated = new idCourseGenerator();
        this.name = name;
        this.assignedCourseRoom = assignedCourseRoom;
        this.teacher = teacher;
        this.id = idgenerated.getId();
    }

    public void addStudent(Student student){
        this.students.add(student);
    }

    public String getName() {
        return this.name;
    }

    public int getAssignedCourseRoom() {
        return this.assignedCourseRoom;
    }

    public List<Student> getStudents() {
        return this.students;
    }

    public Teacher getTeacher() {
        return this.teacher;
    }

    public int getId() {
        return id;
    }
}
