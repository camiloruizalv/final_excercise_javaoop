package Runner;

import University.Course.Course;
import University.University;
import University.User.Student;
import University.User.Teacher;

import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Scanner;

import static AddOns.Printer.linePrinter;

public class Menu{

    public int mainMenuSelector(){
        int optiononmainmenu;
        Scanner scanner = new Scanner(System.in);

        System.out.printf("%s %n", linePrinter("=", 100));
        System.out.printf("%60s %n", "Final work university");
        System.out.printf("%61s %n", "Integrated data system");
        System.out.printf("%s %n%n", linePrinter("=", 100));

        System.out.printf("%5s %n", "1: Print all professors in the university");
        System.out.printf("%5s %n", "2: Print all courses in the university");
        System.out.printf("%5s %n", "3: Create a new student");
        System.out.printf("%5s %n", "4: Create a new course");
        System.out.printf("%5s %n", "5: Display all the enrolled courses of a student");
        System.out.printf("%5s %n%n", "6: Exit");

        System.out.printf("%S", "Please select an option: ");
        optiononmainmenu = scanner.nextInt();

        System.out.printf("%s %n", linePrinter("=", 100));
        return optiononmainmenu;
    }

    public void menuOptionOne(University university){
        List<Teacher> teachers = university.getTeachers();
        System.out.printf("%s %n", linePrinter("=", 100));
        System.out.printf("%60S %n", "Teachers on university");

        System.out.printf("%s %S %n", linePrinter("-", 10), "Full time teachers");
        for (Teacher teacherSelected: teachers) {
            if (Objects.equals(teacherSelected.getPosition(), "Full")){
                System.out.println("Id: " + teacherSelected.getId() + "; Name: " + teacherSelected.getName() + "; Base salary: " + teacherSelected.getBase_salary() + "; Years of experience: " + teacherSelected.getSalaryMultiplier() + "; Salary: " + teacherSelected.calcSalary());
            }
        }

        System.out.printf("%s %S %n", linePrinter("-", 10), "Part time teachers");
        for (Teacher teacherSelected: teachers) {
            if (Objects.equals(teacherSelected.getPosition(), "Part")){
                System.out.println("Id: " + teacherSelected.getId() + "; Name: " + teacherSelected.getName() + "; Base salary: " + teacherSelected.getBase_salary() + "; Active hours: " + teacherSelected.getSalaryMultiplier() + "; Salary: " + teacherSelected.calcSalary());
            }
        }

        System.out.printf("%s %n", linePrinter("=", 100));
    }

    public void menuOptionTwo(University university){
        List<Course> courses = university.getCourses();
        String optiontoactivatecoursesubmenu;
        Scanner scanner = new Scanner(System.in);

        System.out.printf("%s %n", linePrinter("=", 100));

        for (Course courseselected: courses) {
            System.out.println("Id: " + courseselected.getId() + "; Assigned course room: " + courseselected.getAssignedCourseRoom() + "; Name: " + courseselected.getName() + "; Teacher: " + courseselected.getTeacher().getName());
        }

        System.out.printf("%s %n", linePrinter("=", 100));
        System.out.printf("%s", "Do you want to select a course in order to see the full information? y/n ");
        optiontoactivatecoursesubmenu = scanner.nextLine();
        System.out.printf("%s %n", linePrinter("=", 100));

        if (Objects.equals(optiontoactivatecoursesubmenu.substring(0, 1).toLowerCase(Locale.ROOT), "y")) {
            this.subMenuOptionTwo(courses);
        } else if (Objects.equals(optiontoactivatecoursesubmenu.substring(0, 1).toLowerCase(Locale.ROOT), "n")) {
            System.out.println("\n \n");
        } else {
            System.out.printf("%s %n", "The option you selected is wrong, please choose if you want to see the submenu or return to the main screen.");
            this.menuOptionTwo(university);
        }
    }

    public void subMenuOptionTwo(List<Course> courses){
        int optiononsubmenu;
        Scanner scanner = new Scanner(System.in);

        System.out.printf("%s %n", linePrinter("=", 100));

        System.out.printf("%S", "Please select a course by Id: ");
        optiononsubmenu = scanner.nextInt();

        System.out.printf("%s %n", linePrinter("=", 100));

        this.listStudentsOnCourse(courses.get(optiononsubmenu - 1));
    }

    public void listStudentsOnCourse(Course course){
        System.out.printf("%s %n", linePrinter("=", 100));
        System.out.println("Id: " + course.getId() + "; Assigned course room: " + course.getAssignedCourseRoom() + "; Name: " + course.getName() + "; Teacher: " + course.getTeacher().getName());
        System.out.printf("%s %n", "Students on course: ");
        System.out.printf("%s %n", linePrinter("-" , 30));

        for (Student student:course.getStudents()) {
            System.out.println("Id: " + student.getId() + "; Name: " + student.getName());
        }

        System.out.printf("%s %n", linePrinter("=", 100));
    }

    public void menuOptionThree(University university){
        Scanner scanner = new Scanner(System.in);
        String name;
        int age, courseid;
        List<Course> courses = university.getCourses();

        System.out.printf("%s %n", linePrinter("=", 100));
        System.out.printf("%s", "Enter the name of the student: ");
        name = scanner.nextLine();
        System.out.printf("%s", "Enter the age of the student: ");
        age = scanner.nextInt();
        university.createStudent(name, age);
        System.out.printf("%s %n", linePrinter("=", 100));

        for (Course courseselected: courses) {
            System.out.println("Id: " + courseselected.getId() + "; Assigned course room: " + courseselected.getAssignedCourseRoom() + "; Name: " + courseselected.getName() + "; Teacher: " + courseselected.getTeacher().getName());
        }

        System.out.printf("%s %n", linePrinter("=", 100));

        System.out.printf("%s %n", "In which course do you want to enroll the student?");
        System.out.printf("%s", "Please enter the course Id: ");
        courseid = scanner.nextInt();

        university.addStudentToCourse(university.getStudents().size()-1, courseid-1);
    }

    public void menuOptionFour(University university){
        Scanner scanner = new Scanner(System.in);
        String name;
        int assignedCourseRoom;
        int idteacher;
        int idstudent;

        System.out.printf("%s %n", linePrinter("=", 100));
        System.out.printf("%s", "Enter the name of the course: ");
        name = scanner.nextLine();
        System.out.printf("%s", "Enter the assigned room for the course: ");
        assignedCourseRoom = scanner.nextInt();
        System.out.printf("%s %n", linePrinter("=", 100));

        this.menuOptionOne(university);

        System.out.printf("%s %n", linePrinter("=", 100));
        System.out.printf("%s", "Enter the id of the teacher of the course: ");
        idteacher = scanner.nextInt();
        System.out.printf("%s %n", linePrinter("=", 100));

        university.createCourse(name, assignedCourseRoom, university.getTeachers().get(idteacher - 1));

        subMenuOptionFour(university, university.getCourses().size() - 1);
    }

    public void subMenuOptionFour(University university, int idcourse){
        Scanner scanner = new Scanner(System.in);
        Course course = university.getCourses().get(idcourse);
        String idstudents;

        for (Student student: university.getStudents()) {
            System.out.println(university.studentToString(student.getId() - 1));
        }

        System.out.println("Please enter the id's of the students you want to enroll in he course separated by a space");

        idstudents = scanner.nextLine();
        String [] idstudentsseparated = idstudents.split("\\s+");

        for (String id: idstudentsseparated) {
            if (Integer.parseInt(id) - 1 < university.getStudents().size()) {
                university.addStudentToCourse(Integer.parseInt(id) - 1, idcourse);
            } else {
                System.out.println("Option " + id + " is not valid.");
            }
        }
    }

    public void menuOptionFive(University university){
        Scanner scanner = new Scanner(System.in);
        Student studentselected;
        int idstudent;

        for (Student student: university.getStudents()) {
            System.out.println(university.studentToString(student.getId() - 1));
        }

        System.out.printf("%s %n", linePrinter("=", 100));
        System.out.printf("%s", "Please select the id of the student: ");
        idstudent = scanner.nextInt();
        System.out.printf("%s %n", linePrinter("=", 100));


        System.out.println(university.studentToString(idstudent-1));
        for (Course course: university.getCourses()) {
            for (Student student: course.getStudents()) {
                int idstudenttemp = student.getId();
                if (idstudent == idstudenttemp) {
                    System.out.println(university.courseToString(course.getId() - 1));
                }
            }
        }
    }

    public void menuOptionSix(){
        System.out.printf("%s %n", linePrinter("=", 100));
        System.out.printf("%s %n", "Goodbye!!");
        System.out.printf("%s %n", linePrinter("=", 100));
    }
}
