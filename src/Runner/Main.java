package Runner;

import University.University;

public class Main {
    public static void main(String[] args) {

        University university = new University();
        Menu menu = new Menu();

        university.createFullTimeTeacher("Uriel Ruiz", 2500000, 2);
        university.createFullTimeTeacher("Pedro Picapiedra", 1500000, 3);
        university.createPartTimeTeacher("Julian Alvarez", 500000, 12);
        university.createPartTimeTeacher("Patricio Bedoya", 250000, 10);

        university.createStudent("Santiago Cristancho", 20);
        university.createStudent("Veronica Ruiz", 24);
        university.createStudent("Valeria Villa", 22);
        university.createStudent("Steeven Giraldo", 28);
        university.createStudent("Valentina Camacho", 22);
        university.createStudent("Andres Cepeda", 28);

        university.createCourse("OOP fundamentals", 12, university.getTeachers().get(0));
        university.addStudentToCourse(0, 0);
        university.addStudentToCourse(2, 0);
        university.addStudentToCourse(3, 0);

        university.createCourse("TAE advanced topics", 32, university.getTeachers().get(2));
        university.addStudentToCourse(0, 1);
        university.addStudentToCourse(1, 1);
        university.addStudentToCourse(2, 1);
        university.addStudentToCourse(3, 1);

        university.createCourse("Home made Briskets", 2, university.getTeachers().get(1));
        university.addStudentToCourse(1, 2);
        university.addStudentToCourse(3, 2);
        university.addStudentToCourse(2, 2);
        university.addStudentToCourse(4, 2);

        university.createCourse("Astrophysics 1", 1, university.getTeachers().get(2));
        university.addStudentToCourse(0, 3);
        university.addStudentToCourse(1, 3);
        university.addStudentToCourse(2, 3);
        university.addStudentToCourse(3, 3);
        university.addStudentToCourse(4, 3);
        university.addStudentToCourse(5, 3);

        int optionselectedonmainmenu = 0;

        do {
            switch (optionselectedonmainmenu) {
                case 1:
                    menu.menuOptionOne(university);
                    break;
                case 2:
                    menu.menuOptionTwo(university);
                    break;
                case 3:
                    menu.menuOptionThree(university);
                    break;
                case 4:
                    menu.menuOptionFour(university);
                    break;
                case 5:
                    menu.menuOptionFive(university);
                    break;
                case 6:
                    menu.menuOptionSix();
                    break;
            }

            optionselectedonmainmenu = menu.mainMenuSelector();
        } while (optionselectedonmainmenu != 6);
    }
}
