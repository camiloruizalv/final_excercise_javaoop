# README #

This program has 3 main packages <br>

### AddOns ###
This package main purpose is to have the classes that support the main program in some way, in this program the only class that this package has is the "Printer" class.<br>

The Printer class only have a static linePrinter method that helps to print long lines in the console, by concatenating a String type character and returning it to the calling.<br>

### Runner ###
Runner package have two classes (Main and Menu) which helps directly with user interaction.<br>

#### Main ####
Main class have the task of creating all the initial objects (Courses, teachers and students) and comunicating them to the menu class through a switch.<br>

#### Menu ####
Menu class is the main UI class, this class displays the menus to the user and manage the interactions in each method depending on the user choices, it has 6 main methods called on the menu options (menuOptionOne, menuOptionTwo, menuOptionThree....) and 3 other methods to help with the submenus.<br>

##### Option one #####
Displays all teachers in the university by category (Full time teachers and Part time teachers), in order to make this possible the method has two "foreach" who filters all teachers by position using the university.getPosition() method.<br>

##### Option two #####
Display all courses using a "foreach" in the university and asks the user if it wants to display the complete information of a single course, this is filtered using an if statement that calls the listStudentsOnCourse() method in case the user wants to know the entire information of the course.<br>

##### Option Three #####
Allows to create a new Student, with name and age input and later on assigning a course to the new student using the addStudentToCourse() method.<br>

##### Option Four #####
This class helps the user to create a new course, where the user have to input the name, assigned room and teacher for the new course, and then he has to input the id's of the students that are going to take the course, its important to note that in this method there are two types of inputs, name, assigned room and teacher are single type of input and student input are multiple inputs in a single line, separated by single space id's, this is done in the subMenuOptionFour() method by using the .split method on a String and then parsing the single id to a int using Integer.parseInt() method.<br>

##### Option Five #####
This option allows the user to see in which courses a user is enrolled to, this is possible doing a search by id in all courses using two foreach cycles and printing the courses in which the user is enrolled.<br>

##### Option Six #####
THis option close the do-while loop and end the program.<br>

### University ###
This package has three sub-packages, Course, Security and User, and a final main class called University. This is the data handling package in the program.<br>

#### Course ####

Attributes <br>
- private int id <br>
- private String name <br>
- private int assignedCourseRoom <br>
- private List<Student> students <br>
- private Teacher teacher <br>

Constructor<br>
public Course(String name, int assignedCourseRoom, Teacher teacher) -> this allows to first create the course and later on enroll the students to the course.<br>

Method<br>
addStudent: adds a student to the student list.<br>

Getters<br>
- Name -> String<br>
- AssignedRoom -> int<br>
- Students -> List<Student> <br>
- Teacher -> Teacher<br>
- Id -> int<br>

#### Security ####
This package is the responsible on generating the ids of the different types of objects in the program, students, courses and teachers.<br>

Classes:<br>
- idCourseGenerator<br>
- idStudentGenerator<br>
- idTeacherGenerator<br>

All of this classes have the same functionality and the reason to have separated classes for each type of object is to have unique id's in each of them. giving that each object is unique it make sense to have different id's generator to each object type.<br>

Attributes: <br>
- private int id<br>
- private static int counter<br>

Method:<br>
- idCourseGenerator<br>

Getter:<br>
- Id -> int<br>

#### User ####
This package is the responsible on user data handling, more specifically teachers and student data.<br>

##### Teacher #####
Abstract class that sets teacher group, giving the common attributes and methods to the subclasses.<br>

Attributes:<br>
- private int id<br>
- private String name<br>
- private double base_salary<br>
- private String position<br>

Constructor:<br>
- Teacher(String name, double base_salary)<br>

Methods:<br>
- public abstract double calcSalary()<br>

Getters:<br>
- Name -> String<br>
- Base_salary -> double<br>
- Position -> String<br>
- Id -> int<br>
- SalaryMultiplier() -> int = returns the attribute that multiplies the salary (active_hours or years_of_experience)<br>

##### PartTimeTeacher #####
teacher subclass that allows the creation and maintenance of this type of teacher, this class has one aditional attribute active_hours.<br>

Attributes:<br>
- private String position = "Part"<br>
- private int active_hours<br>

##### FullTimeTeacher #####
teacher subclass that allows the creation and maintenance of this type of teacher, this class has one aditional attribute years_of_experience.<br>

Attributes:<br>
- private String position = "Full"<br>
- private int years_of_experience<br>

##### Student #####
Student class that allows the creationd and maintenance of student objects.<br>

Attributes:<br>
- private int id<br>
- private String name<br>
- private int age<br>

Constructor:<br>
- Student(String name, int age)<br>

Getters:<br>
- Id -> int<br>
- Name -> String<br>
- Age -> int<br>

##### University #####

Attributes: <br>
- private List<Teacher> teachers <br>
- private List<Course> courses<br>
- private List<Student> students<br>

Methods: <br>
- createFullTimeTeacher(String name, double base_salary, int years_of_experience)<br>
- createPartTimeTeacher(String name, double base_salary, int active_hours) <br>
- createCourse(String name, int assignedCourseRoom, Teacher teacher) <br>
- addStudentToCourse(int idstudent, int idcourse) <br>
- courseToString(int idcourse)<br>
- createStudent(String name, int age)<br>
- studentToString(int idstudent)<br>

Getters: <br>
- Teachers -> List<Teacher> <br>
- Courses -> List<Course> <br>
- Students -> List<Student> <br>